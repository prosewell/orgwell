# Rules

- All questions must start with `who, what, where, why, when, or how`
- All questions are editable. Questions that are edited will become unsettled
- All questions after adding/editing require a date.
- If a question is edited then the old question and date should be struckout and placed after the updated question and current date.
- All answers need to be preceded by a date.
- Newest answers go at the top of the answers list.

# Orgwell

- [x] What is Orgwell? - 9/2/19
  - **9/2/19**
  - Orgwell is a communal organization/team-focused documentation platform based around the Socratic method where all entries must begin with a question.
- [x] Who are Orgwell's founders - 9/2/19
  - **9/2/19**
  - Jim Yang - a frontend-leaning fullstack developer with experience across numerous industries including ecommerce, adtech, edtech, and healthtech. He also has a background in creative writing.
  - Shon Feder
    - [ ] **What is Shon Feder's background?** - 9/2/19
- [ ] What are Orgwell's values? - 9/2/19

# Roadmap

- [ ] What is the next milestone for the MVP - 9/2/19
  - **9/2/19**
  - Validate the socratic method via markdown

- [x] What is the next milestone for the Beta? - 9/2/19
  - **9/2/19**
  - Porting the relevant Prosewell code to a new Go repo as REST APIs
    - [x] What is the status on this?
    - **9/2/19**
    - Not started
  - Removing Prosewell features that are no longer applicable including all of the front end
    - [x] What is the status on this?
      - **9/2/19**
      - Not started
  - Creating a React client that will access the above APIs
    - [x] What is the status on this?
      - **9/2/19**
      - Not started
  - Creating static pages to publicly display Orgwell's documentation and dogfooding process
    - [x] What is the status on this?
      - **9/2/19**
      - Not started

# Product

- [x] What are Orgwell's core Beta features? - 9/2/19
  - **9/2/19**
  - Question creation/search (homepage)
  - Question answering
  - Question/answer view
  - Landing page
  - Signup/signin
  - Org entity creation
  - Org management (accounts, admin, moderation)
  - Saving question/answer perspectives
  - Email notifications
  - Markdown support
- [x] What is Orgwell's MVP - 9/2/19
  - **9/2/19**
  - Orgwell's MVP is a markdown file that will represent the socratic method of knowledge adding and org decisions. Rules at the top will be expanded to maintain clarity and consistency and provide guidelines to create proper orgwells. The markdown file will need a question asked first before determining what the answer is. This is to validate our hypothesis that questions are a solid foundation for developing documentation

# Engineering

- [x] What is Orgwell's tech stack? - 9/2/19
  - **9/2/19**
  - React/Typescript
  - Go
  - Postgres
  - Google Cloud Platform